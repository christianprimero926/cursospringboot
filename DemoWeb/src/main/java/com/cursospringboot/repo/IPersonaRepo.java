package com.cursospringboot.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursospringboot.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
